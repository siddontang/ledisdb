package ledis

import (
	"github.com/siddontang/ledisdb/store"
)

func (db *DB) Seek(key []byte) (*store.Iterator, error) {
	var minKey []byte
	var err error

	if len(key) > 0 {
		if err = checkKeySize(key); err != nil {
			return nil, err
		}
		minKey = db.encodeKVKey(key)
	} else {
		minKey = db.encodeKVMinKey()
	}

	it := db.bucket.NewIterator()
	it.Seek(minKey)
	return it, nil
}
func (db *DB) KVMaxKey() []byte {
	return db.encodeKVMaxKey()
}

func (db *DB) Key(it *store.Iterator) ([]byte, error) {
	return db.decodeKVKey(it.Key())
}